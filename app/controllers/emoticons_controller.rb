# -*- encoding : utf-8 -*-
class EmoticonsController < ApplicationController
  # GET /emoticons
  # GET /emoticons.json
  helper_method :sort_column, :sort_direction
  def index
    @emoticons = Emoticon.search(params[:search]).order(sort_column + ' ' + sort_direction).paginate(:per_page => 10, :page => params[:page])
  end

  # GET /emoticons/1
  # GET /emoticons/1.json
  def show
    @emoticon = Emoticon.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @emoticon }
    end
  end

  # GET /emoticons/new
  # GET /emoticons/new.json
  def new
    @emoticon = Emoticon.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @emoticon }
    end
  end

  # GET /emoticons/1/edit
  def edit
    @emoticon = Emoticon.find(params[:id])
  end

  # POST /emoticons
  # POST /emoticons.json
  def create
    @emoticon = Emoticon.new(params[:emoticon])

    respond_to do |format|
      if @emoticon.save
        format.html { redirect_to @emoticon, notice: 'Emoticon was successfully created.' }
        format.json { render json: @emoticon, status: :created, location: @emoticon }
      else
        format.html { render action: "new" }
        format.json { render json: @emoticon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /emoticons/1
  # PUT /emoticons/1.json
  def update
    @emoticon = Emoticon.find(params[:id])

    respond_to do |format|
      if @emoticon.update_attributes(params[:emoticon])
        format.html { redirect_to @emoticon, notice: 'Emoticon was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @emoticon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emoticons/1
  # DELETE /emoticons/1.json
  def destroy
    @emoticon = Emoticon.find(params[:id])
    @emoticon.destroy

    respond_to do |format|
      format.html { redirect_to emoticons_url }
      format.json { head :no_content }
    end
  end

  private

  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
