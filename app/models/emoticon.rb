# -*- encoding : utf-8 -*-
class Emoticon < ActiveRecord::Base

scope :unreleased, where(:is_released => false)
after_initialize :default_values


    def default_values
      self.is_released ||= false
      
    end

def self.search(search)
  if search
    where('name LIKE ? or emo LIKE ?', "%#{search}%", "%#{search}%") 
  else
    scoped
  end
end

end
