# -*- encoding : utf-8 -*-
EmoProjekt::Application.routes.draw do

  get "home/index"
  get "start/about"
  # get "users/new"
  # get "sessions/new"
  # get "sign_up" => "users#new", :as => "sign_up"
  # get "log_in" => "sessions#new", :as => "log_in"
  # get "log_out" => "sessions#destroy", :as => "log_out"
  get "emo_list" => "emoticons#index", :as => "emo_list"
  get "add_emo" => "emoticons#new", :as => "add_emo"




  root :to => "home#index"
  # resources :users
  # resources :sessions
  resources :emoticons


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

 
 
  match '/about',   to: 'home#about'
  match 'home' => 'home#index', :as => :welcomePage


end
