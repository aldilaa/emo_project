# -*- encoding : utf-8 -*-
class CreateEmoticons < ActiveRecord::Migration
  def change
    create_table :emoticons do |t|
      t.string :emo
      t.string :name

      t.timestamps
    end
  end
end
