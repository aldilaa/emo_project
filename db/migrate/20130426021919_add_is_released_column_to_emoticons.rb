class AddIsReleasedColumnToEmoticons < ActiveRecord::Migration
  def change
    add_column :emoticons, :is_released, :boolean

  end
end
