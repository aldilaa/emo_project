#encoding: utf-8 
namespace :db do
  desc "Fill compact tables with sample data"
  task data: :environment do
    
    [Emoticon].each(&:delete_all)

    Emoticon.create!(
        name: "laughing",
        emo: "（＾_＾）",
        is_released: true,
        )
    Emoticon.create!(
        name: "troubled",
        emo: "(>_<)>",
        is_released: true,
        )
    Emoticon.create!(
        name: "crying",
        emo: "（ToT）",
        is_released: true,    
        )
    Emoticon.create!(
        name: "apologising",
        emo: "m(_ _)m",
        is_released: false,
        )
    Emoticon.create!(
        name: "shy",
        emo: "(^^;)",
        is_released: true,
        )
    Emoticon.create!(
        name: "grinning",
        emo: "（￣ー￣）",
        is_released: true,
        )
    Emoticon.create!(
        name: "joyful",
        emo: "(≧∇≦)/",
        is_released: true,
        )
    Emoticon.create!(
        name: "surprised",
        emo: "（￣□￣；）",
        is_released: true,
        )
    Emoticon.create!(
        name: "blushed",
        emo: "(#^.^#)",
        is_released: true,
        )
    Emoticon.create!(
        name: "infatuation",
        emo: "（*´▽｀*）",
        is_released: true,
        )
    Emoticon.create!(
        name: "worried",
        emo: "(ーー;)",
        is_released: true,
        )
    Emoticon.create!(
        name: "happy",
        emo: "(*^▽^*)",
        is_released: true,
        )
    Emoticon.create!(
        name: "depressed",
        emo: "＿|￣|○",
        is_released: true,
        )
    Emoticon.create!(
        name: "snubbed",
        emo: "(´･ω･`)",
        is_released: true,
        )
    Emoticon.create!(
        name: "shocked",
        emo: "（　ﾟ Дﾟ）",
        is_released: true,
        )
    Emoticon.create!(
        name: "dissatisfied",
        emo: "(*￣m￣)",
        is_released: true,
        )
    Emoticon.create!(
        name: "mellow",
        emo: "ヽ（´ー｀）┌",
        is_released: true,
        )
    Emoticon.create!(
        name: "awe",
        emo: "（゜◇゜）",
        is_released: true,
        )
    Emoticon.create!(
        name: "giggling",
        emo: "∩( ・ω・)∩",
        is_released: true,
        )
    Emoticon.create!(
        name: "smiling",
        emo: "（＾ｖ＾）",
        is_released: true,
        )
    Emoticon.create!(
        name: "happy",
        emo: "（⌒▽⌒）",
        is_released: true,
        )



  end
end