// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require_tree .
//= require zero-clipboard
//= require zero-clipboard
//= require jquery-ui

 $(document).ready(function() { 
     $("#emos_search input").keyup(function() {
        if ($("#search").val() == "") {
          $("#emos").html("");
          return false;
        }  
        $.get($("#emos_search").attr("action"), $("#emos_search").serialize(), null, "script");
        return false;
      });

      $(document).on('mousedown', '*[data-zc-copy-value]', function(){  
    
       var that = $(this),
           width = that.outerWidth(),
           height =  that.outerHeight();

      
           
        // init new ZeroClipboard client
        clip = new ZeroClipboard.Client();
        clip.setHandCursor( true );
        clip.setText(that.data('zc-copy-value'));
        alert("copied");
       
    });
      var text_input = document.getElementById ('search');
      text_input.focus ();
      text_input.select ();
  });    
