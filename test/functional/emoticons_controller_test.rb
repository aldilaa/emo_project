# -*- encoding : utf-8 -*-
require 'test_helper'

class EmoticonsControllerTest < ActionController::TestCase
  setup do
    @emoticon = emoticons(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emoticons)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emoticon" do
    assert_difference('Emoticon.count') do
      post :create, emoticon: @emoticon.attributes
    end

    assert_redirected_to emoticon_path(assigns(:emoticon))
  end

  test "should show emoticon" do
    get :show, id: @emoticon
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emoticon
    assert_response :success
  end

  test "should update emoticon" do
    put :update, id: @emoticon, emoticon: @emoticon.attributes
    assert_redirected_to emoticon_path(assigns(:emoticon))
  end

  test "should destroy emoticon" do
    assert_difference('Emoticon.count', -1) do
      delete :destroy, id: @emoticon
    end

    assert_redirected_to emoticons_path
  end
end
